import UIKit
import CoreLocation
import WebKit
import AVFoundation

class WebAlert: UIViewController, WKUIDelegate, WKNavigationDelegate, CLLocationManagerDelegate, AVAudioPlayerDelegate, AVAudioRecorderDelegate {
    
    @IBOutlet weak var webView: WKWebView!
    let locationManager = CLLocationManager()
    var isRecording = false
    var audioPlayer: AVAudioPlayer?
    var audioRecorder: AVAudioRecorder?
    var location: String = "Grant access location"
    var locManager = CLLocationManager()
    var currentLocation: CLLocation!
    var player: AVAudioPlayer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.webView.uiDelegate = self
        self.webView.navigationDelegate = self
        
        let url = Bundle.main.url(forResource: "index", withExtension: "html", subdirectory: "gsultan")!
        self.webView.loadFileURL(url, allowingReadAccessTo: url)
        let request = URLRequest(url: url)
        self.webView.load(request)
        
        webView.load(request)
        
        self.locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
        let fileMgr = FileManager.default
        
        let dirPaths = fileMgr.urls(for: .documentDirectory,
                                    in: .userDomainMask)
        
        let soundFileURL = dirPaths[0].appendingPathComponent("\(UUID().uuidString).caf")
        
        let documentsDirectoryURL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let destinationUrl = documentsDirectoryURL.appendingPathComponent(soundFileURL.lastPathComponent)
        
        
        let recordSettings =
            [AVEncoderAudioQualityKey: AVAudioQuality.min.rawValue,
             AVEncoderBitRateKey: 16,
             AVNumberOfChannelsKey: 2,
             AVSampleRateKey: 44100.0] as [String : Any]
        
        let audioSession = AVAudioSession.sharedInstance()
        
        do {
            try audioSession.setCategory(
                AVAudioSession.Category.playAndRecord)
        } catch let error as NSError {
            print("audioSession error: \(error.localizedDescription)")
        }
        
        do {
            try audioRecorder = AVAudioRecorder(url: destinationUrl,
                                                settings: recordSettings as [String : AnyObject])
            audioRecorder?.prepareToRecord()
        } catch let error as NSError {
            print("audioSession error: \(error.localizedDescription)")
        }
        
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() ==  .authorizedAlways {
            currentLocation = locManager.location
            self.location = "Longitude \(currentLocation.coordinate.longitude) Latitude \(currentLocation.coordinate.latitude)"
        }
        
    }
    
    @IBAction func onBtnDismiss(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    private func recordAudio() {
        if !audioRecorder!.isRecording {
            audioRecorder?.record()
        }
    }
    
    private func stopAudio() {
        if audioRecorder!.isRecording {
            audioRecorder?.stop()
            self.savedPath()
        } else {
            audioPlayer?.stop()
        }
    }
    
    private func savedPath() {
        if audioRecorder?.isRecording == false {
            do {
                try audioPlayer = AVAudioPlayer(contentsOf:
                    (audioRecorder?.url)!)
                
                //player = try AVAudioPlayer(contentsOf: audioRecorder!.url, fileTypeHint: AVFileType.caf.rawValue)
                //player!.play()
                
                self.webView.evaluateJavaScript("displayAlert(\"\("Path")\", \"\(audioRecorder!.url)\")") { (_, _) in
                    let fileURL = NSURL(fileURLWithPath: self.audioRecorder!.url.absoluteString)
                    
                    // Create the Array which includes the files you want to share
                    var filesToShare = [Any]()
                    
                    // Add the path of the file to the Array
                    filesToShare.append(fileURL)
                    
                    // Make the activityViewContoller which shows the share-view
                    let activityViewController = UIActivityViewController(activityItems: filesToShare, applicationActivities: nil)
                    
                    // Show the share-view
                    self.present(activityViewController, animated: true, completion: nil)
                }
                
                
                
                //webView.evaluateJavaScript("displayAlert(\"\("Path")\", \"\(audioRecorder!.url)\")", completionHandler: nil)
            } catch let error as NSError {
                print("audioPlayer error: \(error.localizedDescription)")
            }
        }
    }
    
    private func getLoacation() {
        webView.evaluateJavaScript("displayAlert(\"\("Location")\", \"\(self.location)\")", completionHandler: nil)
    }
    
    
    func webView(_ webView: WKWebView, runJavaScriptAlertPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo,
                 completionHandler: @escaping () -> Void) {
        
        switch message {
        case "startRecording()":
            self.isRecording = true
            self.recordAudio()
            completionHandler()
        case "stopRecording()":
            self.stopAudio()
            completionHandler()
        case "getCoordinates()":
            self.getLoacation()
            completionHandler()
        default:
            let alertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                completionHandler()
            }))
            
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
}

