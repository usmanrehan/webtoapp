//
//  SideMenuTVC.swift
//  WebToApp
//
//  Created by Mac Book on 18/05/2020.
//  Copyright © 2020 Sherdle. All rights reserved.
//

import UIKit

class SideMenuTVC: UITableViewCell {

    @IBOutlet weak var viewSeparator: UIView!
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var viewSelected: UIView!
    
    let blue = UIColor(displayP3Red: 17/255, green: 106/255, blue: 177/255, alpha: 1.0)
    
    func setData(data:SideMenuData){
        self.lblTitle.text = data.title
        self.viewSeparator.isHidden = !data.showSeparator
        self.viewSelected.isHidden = !data.isSelected
        self.imgIcon.image = UIImage(named: data.title.lowercased())
        
        switch data.isSelected{
        case true:
            self.lblTitle.textColor = self.blue
            self.imgIcon.image = self.imgIcon.image?.tinted(with: self.blue)
        case false:
            self.lblTitle.textColor = .darkGray
            self.imgIcon.image = self.imgIcon.image?.tinted(with: .darkGray)
        }
    }

}
extension UIImage {
    func tinted(with color: UIColor, isOpaque: Bool = false) -> UIImage? {
        let format = imageRendererFormat
        format.opaque = isOpaque
        return UIGraphicsImageRenderer(size: size, format: format).image { _ in
            color.set()
            withRenderingMode(.alwaysTemplate).draw(at: .zero)
        }
    }
}
